<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

$active_group = 'default';
$active_record = TRUE;

$db['default']['hostname'] = 'localhost';
$db['default']['username'] = 'root';
$db['default']['password'] = '';
// $db['default']['port'] = 23306;
$db['default']['database'] = 'c2_db_smart';
$db['default']['dbdriver'] = 'mysqli';
$db['default']['dbprefix'] = '';
$db['default']['pconnect'] = TRUE;
$db['default']['db_debug'] = TRUE;
$db['default']['cache_on'] = FALSE;
$db['default']['cachedir'] = '';
$db['default']['char_set'] = 'utf8';
$db['default']['dbcollat'] = 'utf8_general_ci';
$db['default']['swap_pre'] = '';
$db['default']['autoinit'] = TRUE;
$db['default']['stricton'] = FALSE;


$db['second']['hostname'] = 'localhost';
$db['second']['username'] = 'root';
$db['second']['password'] = '';
// $db['default']['port'] = 23306;
$db['second']['database'] = 'c2_db_smart';
$db['second']['dbdriver'] = 'mysqli';
$db['second']['dbprefix'] = '';
$db['second']['pconnect'] = TRUE;
$db['second']['db_debug'] = TRUE;
$db['second']['cache_on'] = FALSE;
$db['second']['cachedir'] = '';
$db['second']['char_set'] = 'utf8';
$db['second']['dbcollat'] = 'utf8_general_ci';
$db['second']['swap_pre'] = '';
$db['second']['autoinit'] = TRUE;
$db['second']['stricton'] = FALSE;


/* End of file database.php */
/* Location: ./application/config/database.php */