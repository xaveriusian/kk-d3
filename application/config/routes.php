<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

$route['default_controller'] = "landpage";
$route['404_override'] = '';
$route['beasiswa_s2'] = 'landpage';
$route['kk'] = 'landpage/kk';
$route['terimakasih-rapor'] = 'landpage/trims';
$route['kk/(:any)'] = 'landpage/trims/$1';


/* End of file routes.php */
/* Location: ./application/config/routes.php */
?>